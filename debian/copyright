This is the Debian GNU/Linux r-cran-conquer package of conquer.  The
conquer package offers convolution-type smoothed quantile regression.
It was written by Xuming He, Xiaoou Pan, Kean Ming Tan, and Wen-Xin
Zhou.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from the main CRAN site
	https://cran.r-project.org/src/contrib/
and are also available from all CRAN mirrors as e.g.
	https://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'conquer' to
'r-cran-conquer' to fit the pattern of CRAN (and non-CRAN)
packages for R.

Files: *
Copyright: 2020 -   Xuming He, Xiaoou Pan, Kean Ming Tan, and Wen-Xin Zhou
License: GPL-3

Files: debian/*
Copyright: 2020  Dirk Eddelbuettel <edd@debian.org>
License: GPL-2+

On a Debian GNU/Linux system, the GPL license (version 2) is included
in the file /usr/share/common-licenses/GPL-2 and the GPL license
(version 3) is included in the file /usr/share/common-licenses/GPL-3

For reference, the upstream DESCRIPTION file is included below:

   Package: conquer
   Type: Package
   Title: Convolution-Type Smoothed Quantile Regression
   Version: 1.0.1
   Date: 2020-04-11
   Authors@R: c(person("Xuming", "He", email = "xmhe@umich.edu", role = "aut"),
                person("Xiaoou", "Pan", email = "xip024@ucsd.edu", role = c("aut", "cre")), 
                person("Kean Ming", "Tan", email = "keanming@umich.edu", role = "aut"),
                person("Wen-Xin", "Zhou", email = "wez243@ucsd.edu", role = "aut"))
   Description: Fast and accurate convolution-type smoothed quantile
     regression. Implemented using Barzilai-Borwein gradient descent
     with a Huber regression warm start. Construct confidence intervals
     for regres sion coefficients using multiplier bootstrap.
   Depends: R (>= 3.6.0)
   License: GPL-3
   Encoding: UTF-8
   URL: https://github.com/XiaoouPan/conquer
   SystemRequirements: C++11
   Imports: Rcpp (>= 1.0.3), Matrix, matrixStats, stats
   LinkingTo: Rcpp, RcppArmadillo
   RoxygenNote: 7.1.0
   NeedsCompilation: yes
   Packaged: 2020-05-06 05:09:55 UTC; xopan
   Author: Xuming He [aut],
     Xiaoou Pan [aut, cre],
     Kean Ming Tan [aut],
     Wen-Xin Zhou [aut]
   Maintainer: Xiaoou Pan <xip024@ucsd.edu>
   Repository: CRAN
   Date/Publication: 2020-05-06 12:40:09 UTC


